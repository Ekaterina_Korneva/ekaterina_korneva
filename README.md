

---

# Екатерина Корнева
[![Telegram](https://img.shields.io/badge/Telegram-1a3136?style=social&logo=telegram)](https://t.me/ekaterina_korneva)
[![Gmail](https://img.shields.io/badge/Gmail-1a3136?style=social&logo=gmail)](mailto:zhivotova91@gmail.com)

## Тестировщик (ручное + автоматизация)

[:point_right: Резюме](https://drive.google.com/file/d/1Z-C2V3cRYw3q51p-d_LMMwwzVcI1g6Zp/view?usp=sharing)

## Ручное тестирование (Web + Mobile)

![DevTools](https://img.shields.io/badge/DevTools-122529?style=for-the-badge&logo=googlechrome)
![Postman](https://img.shields.io/badge/Postman-122529?style=for-the-badge&logo=postman&logoColor=f76935)
![Swagger](https://img.shields.io/badge/Swagger-122529?style=for-the-badge&logo=swagger&logoColor=7ede2b)
![Curl](https://img.shields.io/badge/Curl-122529?style=for-the-badge&logo=curl&logoColor=7ede2b)
![SOAPUI](https://img.shields.io/badge/SOAPUI-122529?style=for-the-badge&logo=soapui&logoColor=d4088d)
![GitLab](https://img.shields.io/badge/GitLab_Issues-122529?style=for-the-badge&logo=gitlab)
![GoogleSheets](https://img.shields.io/badge/Google%20Sheets-122529?style=for-the-badge&logo=google-sheets)
![Figma](https://img.shields.io/badge/Figma-122529?style=for-the-badge&logo=figma&logoColor=7d5fa6)
![MySQL](https://img.shields.io/badge/MySQL-122529?style=for-the-badge&logo=mysql)
![PostgreSQL](https://img.shields.io/badge/PostgreSQL-122529?style=for-the-badge&logo=postgresql)
![Dbeaver](https://img.shields.io/badge/Dbeaver-122529?style=for-the-badge&logo=dbeaver)

### Тестовые артефакты

- [Чек-листы](https://docs.google.com/spreadsheets/d/1vop3IS5i0RXub-nAmLZVMj9ocaQoVwHIMek7hEDhra0/edit#gid=0)
- [Тест-кейсы](https://docs.google.com/spreadsheets/d/1ktdDqv5lJuLLZN2JGxbszsoGHH_jDeKUnehvGdmIBDM/edit#gid=0)
- [Баг-репорты](https://drive.google.com/drive/folders/1c1n9CdvW7z59HbIIMj2Gu5Rmt-bKyeMl)

## Автоматизация
![Visual Studio Code](https://img.shields.io/badge/Visual%20Studio%20Code-122529?style=for-the-badge&logo=visual-studio-code&logoColor=0080FF)
![Playwright](https://img.shields.io/badge/Playwright-122529?style=for-the-badge&logo=playwright)
![TypeScript](https://img.shields.io/badge/typescript-122529?style=for-the-badge&logo=typescript)
![Git](https://img.shields.io/badge/Git-122529?style=for-the-badge&logo=git)
![GitLab](https://img.shields.io/badge/GitLab-122529?style=for-the-badge&logo=gitlab)

## Сертификаты
Школа Смартап - [удостоверение о переквалификации](ссылка)<br>
Томский Государственный Университет - [удостоверение о переквалификации](ссылка)